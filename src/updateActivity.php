<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="../css/evenspaces.css" />

    <?php
include_once("menu.php");
?>

    <script>

        var id;

        $(document).ready(function(){
            
            $.ajax({
                type: "POST",
                url: "../api/activity/read.php",
                success: function(data) {
                    var htmlForTable = '';
                    console.log(data);

                    data.records.forEach(function(element) {
                        
                        if(element.id == <?php echo $_GET['id'];?>){
                            id = element.id;
                            $("#acName").val(element.name);
                            $("#acCity").val(element.location);
                            $("#acDesc").val(element.description);
                            $("#acLength").val(element.length);
                            $("#acDuration").val(element.duration);
                            
                        }
                    });
                }
            });
            
            $('#updateButton').click(function(){
                var form_data = new FormData();
            
                form_data.append('acName', $('#acName').val());
                form_data.append('acDesc', $('#acDesc').val());
                form_data.append('acLength', $('#acLength').val());
                form_data.append('acCity', $('#acCity').val());
                form_data.append('acDuration', $('#acDuration').val());
                form_data.append('id', id);

                
                $.ajax({
                    url: '../api/activity/update.php', // point to server-side PHP script
                    dataType: 'text', // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function (data) {
                        window.location.href = 'myactivities.php';
                    }
                });

            });


$(document).on("click", "#deleteButton", function(event){
    var form_data = new FormData();
            
                form_data.append('acName', $('#acName').val());
                form_data.append('acDesc', $('#acDesc').val());
                form_data.append('acLength', $('#acLength').val());
                form_data.append('acCity', $('#acCity').val());
                form_data.append('id', id);

                
                $.ajax({
                    url: '../api/activity/delete.php', // point to server-side PHP script
                    dataType: 'text', // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function (data) {
                        window.location.href = 'myactivities.php';
                    }
                });
});
            

        });


    </script>
</head>
<body background="..\img\background.jpg" style="background-size: cover">



<h1>Enter a new activity:</h1>
    <!--<img src="https://cdn-a.william-reed.com/var/wrbm_gb_food_pharma/storage/images/7/6/1/2/952167-1-eng-GB/Some-sugary-drinks-can-help-improve-endurance-Study_wrbm_large.jpg" align="right" height="45%">-->
    <form method="POST" enctype="multipart/form-data" class="form-horizontal">
        
             <div class="form-group">        
            <label for="name"  class="control-label col-sm-2"><strong>Name of activity:</strong> </label>
            <div class="col-sm-5">
            <input type="text" name="name" placeholder="For Example: myactivity" size=40 class="form-control" id="acName">
           </div>
           </div>

          <div class="form-group">
            <label for="city"  class="control-label col-sm-2"><strong>Location:</trong> </label>
            <div class="col-sm-5">
            <input type="text" id="acCity" name="name" class="form-control" placeholder="For Example: Munich" size=40>
            </div>
            </div>

            <div class="form-group">
            <label for="description" class="control-label col-sm-2"><strong>Description:</strong> </label>
            <div class="col-sm-5">
            <textarea name="description" cols=40 rows=6 id="acDesc" class="form-control" placeholder="For Example: In the forest, only suitable for jogging if the weather is dry"></textarea>
            </div>
            </div>

            <div class="form-group">
            <label for="length"  class="control-label col-sm-2"><strong>Route(km):</strong> </label>
            <div class="col-sm-5">
            <input type="number" name="name" id="acLength" class="form-control" placeholder="6" size=6>
            </div>
            </div>

             <div class="form-group">
            <label for="length"  class="control-label col-sm-2"><strong>Duration(min):</strong> </label>
            <div class="col-sm-5">
            <input type="number" name="name" id="acDuration" class="form-control" placeholder="30" size=6>
            </div>
            </div>

            <div class="form-group" style="position: relative">
            <div class="col-sm-5">
            <input type="button" class="btn btn-info btn-lg" style="position: relative; top: 10px;" data-toggle="modal" data-target="#updateModal" value ="Update Activity" id="modalButton">
            </div>
            <div class="col-sm-5">
            <input type="button" class="btn btn-info btn-lg" style="position: relative; top: 10px; left: 20px" data-toggle="modal" data-target="#deleteModal" value ="Delete Activity" id="modalButton">
            </div>
            </div>
       




    </form>

    <div id="updateModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" align="left">Update record?</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to update the record?</p>
      </div>
      <div class="modal-footer">
      <form>
        <button type="submit" class="btn btn-default" data-dismiss="modal" id="updateButton">Yes</button>
        <button type="button" class="btn" data-dismiss="modal">No</button>
        </form>
      </div>
    </div>

  </div>
</div>


    <div id="deleteModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" align="left">Delete record?</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete the record?</p>
      </div>
      <div class="modal-footer">
      <form>
        <button type="submit" class="btn btn-default" data-dismiss="modal" id="deleteButton">Yes</button>
        <button type="button" class="btn" data-dismiss="modal">No</button>
        </form>
      </div>
    </div>

  </div>
</div>

</body>
</html>