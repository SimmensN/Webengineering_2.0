<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>RunDiary</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/main.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <?php
include_once("menu.php");
?>

    <script src="../js/jquery-3.3.1.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $.ajax({
                type: "POST",
                url: "../api/activity/read.php",
                success: function(data) {
                    var htmlForTable = '';
                    console.log(data);
                    data.records.forEach(function(element) {
                        if(element.visible == '1'){
                        htmlForTable += '<tr><td>' + element.name + '</td><td>' + element.location + '</td><td>' + element.description + '</td><td>'  + element.length + '</td><td>'+ element.duration +'</td><td>' + element.createdate + '</td><td>' + element.username  + "</td><td><a href=\""+ element.file + "\"><button class=\"btn btn-success\">Download file</button></a></td></tr>";
                        $("#activities").append(htmlForTable);
                        htmlForTable = '';
                        }
                    });
                }
            });

        $('#filterButton').click(function(){
          $.ajax({
                type: "POST",
                url: "../api/activity/read.php",
                success: function(data) {
                    var htmlForTable = '';
                    $filterText = $('#filterText').val();
                    $('#activities td').remove();
                    console.log(data);
                    data.records.forEach(function(element) {
                      if(element.name.includes($filterText) && element.visible == '1'){
                        htmlForTable += '<tr><td>' + element.name + '</td><td>' + element.location + '</td><td>' + element.description + '</td><td>'  + element.length + '</td><td>'+ element.duration + '</td><td>' + element.createdate + '</td><td>' + element.username  + "</td><td><a href=\""+ element.file + "\">Download file</a></td></tr>";                        
                        $("#activities").append(htmlForTable);
                        htmlForTable = '';
                    }
                    });
                }
            });


        });
        });

    </script>
</head>
<body background="..\img\background.jpg" style="background-size: cover">



<h1>All Activities:</h1><br>

<div id="target" class="container">

<input type="text" placeholder="Type in filter" class ="form-control" id="filterText">
<input type="button" value="Filter" class="btn btn-primary" id="filterButton"style="margin: 10px">
<table id = "activities" class="table table-striped">
<thead>
 <tr>
 <th>Name</th>
 <th>Location</th>
 <th>Description</th>
 <th>Length</th>
 <th>Duration</th>
 <th>Created at</th>
 <th>Created by</th>
 <th>KML-File</th>
 </tr>
 <thead>
 <tbody>
 
 </tbody>
</table>
</div>
<!--Check, if user is logged in. If not, log out user-->

</body>
</html>