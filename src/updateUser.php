<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Update Profile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<link rel="stylesheet" type="text/css" media="screen" href="../css/main.css" /> -->
   <!-- <link rel="stylesheet" type="text/css" media="screen" href="../css/evenspaces.css" /> -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <?php
include_once("menu.php");
?>

    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script>

var id;

$(document).ready(function(){

    $('#updateButton').click(function(){
        var form_data = new FormData();
    
        form_data.append('us_username', $('#us_username').val());
        form_data.append('us_mail', $('#us_mail').val());
        form_data.append('us_firstname', $('#us_firstname').val());
        form_data.append('us_lastname', $('#us_lastname').val());
        //form_data.append('us_oldpass', $('#us_oldpass').val());
        form_data.append('us_newpass', $('#us_newpass').val());
       // form_data.append('id', $_SESSION['id']);

        
        $.ajax({
            url: '../api/activity/updateUserDatabase.php', // point to server-side PHP script
            dataType: 'text', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (data) {
                var successField = document.getElementById('successDiv');
                successField.style.display = 'block';
            },
           error: function(data){
                var errorField = document.getElementById('errorDiv');
                errorField.style.display = 'block';
            }
        });

    });

});

</script>

</head>
<body background="..\img\background.jpg">
<div class="alert alert-success" style="display: none" id="successDiv">
  <strong>Success!</strong> User data changed.
</div>

<div class="alert alert-danger" style="display: none" id="errorDiv">
  <strong>Error!</strong> Operation could not be completed. Please try again.
</div>

 <form  method="POST" enctype="multipart/form-data" class="form-horizontal">
                <legend>
                    <b>Change your User-Data!!! Hurray!!!</b>
                </legend>
            
                    <div class="form-group">
                    <label for="username" class="control-label col-sm-2">Username: </label>
                    <div class="col-sm-10">
                    <input type="text" size="15" name="username" id="us_username" class="form-control">
                    </div>
                    </div>

                    <div class="form-group">
                    <label for="firstname" class="control-label col-sm-2">First Name:</label>
                    <div class="col-sm-10">
                    <input type="text" size="15" name="firstname" id="us_firstname" class="form-control">
                    </div>
                    </div>

                    <div class="form-group">
                    <label for="lastname" class="control-label col-sm-2">Last Name:</label>
                    <div class="col-sm-10">
                    <input type="text" size="15" name="lastname" id="us_lastname" class="form-control">
                    </div>
                    </div>

                    <div class="form-group">
                    <label for="newpassword" class="control-label col-sm-2">New Password:</label>
                    <div class="col-sm-10">
                    <input type="password" size="15" name="newpassword" id="us_newpass" class="form-control">
                    </div>
                    </div>

                

                    <div class="form-group">
                    <label for="mail" class="control-label col-sm-2">Mail:</label>
                    <div class="col-sm-10">
                    <input type="text" size="15" name="mail" id="us_mail" class="form-control">
                    </div>
                    </div>
                    <!--<input type="checkbox" name="alreadyregistered" value="true" onclick="changeState('this')" id="isregistered"> I am already registered.-->
                    <!--<input id='hiddenCheckbox' type='hidden' value='false' name='alreadyregistered'> -->
                    <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                    <input type="button" value ="Update Profile" id="updateButton" class="btn btn-success">
                    </div>
                    </div>
              
            </form>

            <img src="..\img\joggen.jpg" class="img-rounded" alt="[Bild = joggen.jpg]" style="opacity: 0.3" style="margin-left: auto; margin-right: auto; display: block">
            
    
</body>
</html>