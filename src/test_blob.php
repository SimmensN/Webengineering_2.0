<HTML>
<HEAD>
<TITLE>Upload Image to MySQL BLOB</TITLE>

</HEAD>
<BODY>

<?php
session_start();
if(isset($_SESSION['userid'])) {
$userid = $_SESSION['userid'];
}


print_r($_FILES);

$target_dir = "/web_eng/img/$userid/";
$target_file = $target_dir . basename($_FILES["file"]["name"]);
$uploadOk = 1;


$inipath = php_ini_loaded_file();

if ($inipath) {
    echo 'Loaded php.ini: ' . $inipath;
} else {
   echo 'A php.ini file is not loaded';
}

if (!file_exists($target_dir)) {
    mkdir($target_dir, 0777, true);
}

$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image

if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["file"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["file"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "pdf" && $imageFileType != "html"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
error_reporting(-1);
} else {
    if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["file"]["tmp_name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file." . $_FILES["file"]["error"];
        echo $_FILES["file"]["tmp_name"];
        echo "   ";
        echo $target_file;
        echo "   ";
        echo $target_dir;
    }
}



?>

<form enctype="multipart/form-data" action="test_blob.php" method="post">
    <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
    Upload this file: <input type="file" name="file" />
    <input type="submit" value="Submit File" />
</form>
</div>
</BODY>
</HTML>