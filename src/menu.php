<!-- 
This code snipped provides the menu and is included in every other php file that is 
displayed to the user.
    -->
<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>
<link rel="shortcut icon" type="image/x-icon" href="..\img\logo.png" />
<nav class="navbar navbar-expand-sm bg-primary navbar-dark">
<div class="container-fluid">
<div class="navbar-header">
    <a class="navbar-brand" href="index.php">Rundiary</a>
</div>


<?php

if(isset($_SESSION['userid'])) {
    ?>
<ul class="nav navbar-nav">
<li class="nav-item"><a href="index.php" class="nav-link">Home</a></li>
<li class="nav-item"><a href="overview.php" class="nav-link">Overview</a></li>
<li class="nav-item"><a href="myactivities.php"  class="nav-link">My Activities</a></li>
<li class="nav-item"><a href="statistics.php"  class="nav-link">Statistics</a></li>
<li class="nav-item"><a href="newactivity.php"  class="nav-link">New Activity</a></li>
<!--<li class="nav-item"><a href="login.php"  class="nav-link">Login/Register</a></li>-->
<li class="nav-item"><a href="updateUser.php"  class="nav-link">Update Profile</a></li>
<li class="nav-item"><a href="logout.php" class="nav-link">Logout</a></li>
<li class="nav-item"><a href="impressum.php"  class="nav-link">Impressum</a></li>
</ul>
<?php
}else{
?>
<ul class="nav navbar-nav">
<li class="nav-item"><a href="index.php" class="nav-link">Home</a></li>
<li class="nav-item"><a href="overview.php" class="nav-link">Overview</a></li>
<li class="nav-item"><a href="login.php" class="nav-link">Login/Register</a></li>
<li class="nav-item"><a href="impressum.php" class="nav-link">Impressum</a></li>

</ul>
<?php
}
?>
</div>
</nav>
