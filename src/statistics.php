<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Statistics</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/main.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <?php
include_once("menu.php");
?>

    <script src="../js/Chart.js"></script>
    <script src="../js/jquery-3.3.1.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>


   <script>
        var all = [];
        var alldata = [];
        var alldur = [];
        var allloc = [];
        var resultarr = [];
        $(document).ready(function(){
            $.ajax({
                type: "POST",
                url: "../api/activity/read.php",
                success: function(data) {


                    var htmlForTable = '';
                    var totallength = 0;
                    var cities = "";
                    var dates = "";



                    console.log(data);
                    data.records.forEach(function(element) {
                        //if(element.visible == '0'){
                        totallength += parseInt(element.length);
                        if(!cities.includes(element.location)){
                            cities += " - " + element.location + "<br>";
                        }
                        dates += " - " + element.createdate + "<br>";
                        all.push(element.createdate + " -- " + element.name);
                        alldata.push(element.length);
                        allloc.push(element.location);
                        alldur.push(element.duration);
                        console.log(totallength);
                        console.log(cities);
                        console.log(dates);
                        //}
                    });
                    htmlForTable += '<tr><td>' + totallength + '</td></tr>';
                        $("#activities").append(htmlForTable);
                        htmlForTable = '';
                        createChart();
                }
            });

        });


function createChart(){
var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: all,
        datasets: [{
            label: '# of kilometers',
            data: alldata,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});



var ctxsec = document.getElementById("myChartsecond").getContext('2d');
var mysecondChart = new Chart(ctxsec, {
    type: 'bar',
    data: {
        labels: all,
        datasets: [{
            label: '# minutes',
            data: alldur,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});





}



</script>

</head>
<body background="..\img\background.jpg" style="background-size: cover">


<div id="target" class="container">

<h1>When did I jog what distance?</h1>
<canvas id="myChart" width="200" height="80"></canvas>
<h1>How long did I jog?</h1>
<canvas id="myChartsecond" width="200" height="80"></canvas>

<table id = "activities" class="table table-striped">
<thead>
 <tr>
 <th>Total length (km)</th>
 </tr>
 <thead>
 <tbody>
 
 </tbody>
</table>
</div>

    
</body>
</html>