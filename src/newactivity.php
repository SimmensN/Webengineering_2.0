<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>RunDiary</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<link rel="stylesheet" type="text/css" media="screen" href="../css/main.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="../css/evenspaces.css" /> -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <?php
include_once("menu.php");
?>

    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script>
        function uploadFile() {
            if ($("#file_to_upload").val() != "") {
                var file_data = $('#file_to_upload').prop('files')[0];
                var form_data = new FormData();

           

                form_data.append('file', file_data);
                form_data.append('acName', $('#acName').val());
                form_data.append('acDesc', $('#acDesc').val());
                form_data.append('acLength', $('#acLength').val());
                form_data.append('acCity', $('#acCity').val());
                form_data.append('acDuration', $('#acDuration').val());

                if(document.getElementById('acCheck').checked){
                    form_data.append('acCheck', '1');
                }else{
                    form_data.append('acCheck', '0');
                }
               


                $.ajax({
                    url: 'uploadFile.php', // point to server-side PHP script
                    dataType: 'text', // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function (data) {
                        // get server responce here
                    
                        $("#file_to_upload").val("");
                        var successField = document.getElementById('successDiv');
                        successField.style.display = 'block';
                    },
                    error: function(data){
                        var errorField = document.getElementById('errorDiv');
                        errorField.style.display = 'block';
                    }
                });
            } else {
                var errorField = document.getElementById('errorDiv');
                errorField.style.display = 'block';
            }
        }
    </script>
    

</head>

<body style="background-size: cover" background="https://hbee178.files.wordpress.com/2013/06/white-blue-effect-backgrounds-for-powerpoint.jpg?" style="background-size:cover">
        
        
        <div class="alert alert-success" id="successDiv" style="display:none">
        <strong>Success!</strong> New Record created.
        </div>

        <div class="alert alert-danger" id="errorDiv" style="display:none">
        <strong>Error!</strong> Please try again.
        </div>
    <!--Check, if user is logged in. If not, log out user-->

    <h1>Enter a new activity:</h1>
    <!--<img src="https://cdn-a.william-reed.com/var/wrbm_gb_food_pharma/storage/images/7/6/1/2/952167-1-eng-GB/Some-sugary-drinks-can-help-improve-endurance-Study_wrbm_large.jpg" align="right" height="45%">-->
    <form method="POST" enctype="multipart/form-data" class="form-horizontal">
        <div>
        <div class="form-group">
            <label for="name" class="control-label col-sm-2"><strong>Name of activity:</strong> </label>
            <div class="col-sm-5">
            <input type="text" name="name" placeholder="For Example: myactivity" size=40 id="acName" class="form-control">
            </div>
            </div>

            <div class="form-group">
            <label for="city" class="control-label col-sm-2"><strong>Location:</strong> </label>
            <div class="col-sm-5">
            <input type="text" id="acCity" name="name" placeholder="For Example: Munich" size=40 class="form-control">
            </div>
            </div>
            
            <div class="form-group">
            <label for="description" class="control-label col-sm-2"><strong>Description:</strong> </label>
            <div class="col-sm-5">
            <textarea name="description" cols=40 rows=6 id="acDesc"class="form-control"  placeholder="For Example: In the forest, only suitable for jogging if the weather is dry"></textarea>
            </div>
            </div>

            <div class="form-group">
            <label for="length" class="control-label col-sm-2"><strong>Distance(km):</strong> </label>
            <div class="col-sm-5">
            <input type="number" name="name" id="acLength" placeholder="6" size=6 class="form-control">
            </div>
            </div>

             <div class="form-group">
            <label for="length" class="control-label col-sm-2"><strong>Duration(min):</strong> </label>
            <div class="col-sm-5">
            <input type="number" name="name" id="acDuration" placeholder="45" size=6 class="form-control">
            </div>
            </div>
            
            
            <div class="form-group">
            <label for="coordinates" class="control-label col-sm-2"><strong>Route(.kml):</strong> </label>
            <div class="col-sm-5">
            <input type="file" name="coordinates" placeholder="6" size=6 id="file_to_upload" class="form-control">
            </div>
            </div>
            
            <div class="form-group">
            <label for="visibleforallusers" class="control-label col-sm"><strong>Visible for all users:</strong> </label>
            <div class="col-sm-5">
            <input type="checkbox" name="visibleforallusers" value="1" id="acCheck">
            </div>
            </div>
            

        <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
        <button onclick="uploadFile()" class="btn btn-success">Submit</button>
        </div>
        </div>


    </form>
</body>

</html>