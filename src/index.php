<!-- 
Start site
    -->

<!DOCTYPE <!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>RunDiary</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/main.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css">

       <?php
include_once("menu.php");
?>
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <style>
.carousel {
  width:auto;
  height: 300px;
  overflow: hidden;

}

iframe{
    margin-top: 20px;
    position: right;
}




    </style>
</head>

<body>

    <body>




<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
  </ol>

<div id="carouselExampleControls" class="carousel slide" data-ride="carousel" id="carusel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="..\img\01.jpeg" alt="Running 01">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="..\img\02.jpg" alt="Running 02">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>  
</div>

<div class="container">
<h1 style="margin-top: 15px">Lauf dich fit!</h1>
<div class="row">
<div class="col-sm-6">
<iframe width="560" height="315" src="https://www.youtube.com/embed/DnLpQjd0j-s" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
  </div>

  <div class="col-sm-6" style="margin-top: 20px">
  Jogging-Sünden wiegen schwer, denn: Jogging zählt zu den beliebtesten Ausdauersportarten. Zehn Millionen Menschen schnüren alleine in Deutschland regelmäßig ihre Laufschuhe und werfen ihre Laufjacke über. <br><br>Aus gutem Grund: Es ist gesund, kräftigt Herz und Kreislauf, verhindert Übergewicht und macht obendrein Spaß. Also, Schuhe an und los?<br><br>

Ganz so einfach ist es eben leider nicht! Besonders Anfänger übertreiben bei den ersten Trainingseinheiten. In der Vorstellung, schnell Gesundheit und Fitness zu tanken, vernachlässigen sie, was Ausdauertraining ausmacht: nämlich Ausdauer.

Zu viel, zu oft, zu schnell – das sind die häufigsten Jogging-Sünden, die nicht selten zu Verletzungen führen. <br><br>Denn im Gegensatz zur Muskulatur benötigen unsere Knochen, Gelenke, Sehnen und Bänder mehrere Wochen, bis sie die Laufbelastung tolerieren. „Wie das optimale Laufen für Anfänger genau aussieht, ist je nach Belastung und auch individuell sehr unterschiedlich. <br><br>Nehmen Sie sich deshalb die Zeit herauszufinden, wie Ihr Körper auf das Laufen reagiert“, rät Prof. Dr. Thomas Wessinghage, ehemaliger Weltklasse-Läufer und Ärztlicher Direktor der drei Medical Park-Kliniken im Tegernseer Tal.  <div>
    
  </div>
  </div>
  </div>
</div>
<hr style="height:95px; border:none;color:#333;" class="bg-primary" />
    </body>

</html>