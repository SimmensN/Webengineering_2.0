<!-- 
Login Site displayed to the user.
    -->
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>RunDiary</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/bootstrap.min.css">

      <?php
    include_once("menu.php");
    ?>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script>

var id;

$(document).ready(function(){
    
    
    $('#loginButton').click(function(){

            var form_data = new FormData();
            
            form_data.append('acName', $('#acName').val());
            form_data.append('acDesc', $('#acDesc').val());
            form_data.append('acLength', $('#acLength').val());
            form_data.append('acCity', $('#acCity').val());
            form_data.append('id', id);

          $.ajax({
                type: "POST",
                url: "../api/activity/read.php",
                success: function(data) {
                    var htmlForTable = '';
                    $filterText = $('#filterText').val();
                    $('#activities td').remove();
                    console.log(data);
                    data.records.forEach(function(element) {
                      if(element.name.includes($filterText)){
                        htmlForTable += '<tr id=' + element.id +'><td>' + element.name + '</td><td>' + element.location + '</td><td>' + element.description + '</td><td>'  + element.length + '</td><td>'+ element.duration + '</td><td>' + element.createdate + '</td><td>' + element.username  + "</td><td><a href=\""+ element.file + "\"><button class=\"btn btn-success\">Download file</button></a></td><td><a href=\"updateActivity.php?id="+ element.id +"\"><button class=\"btn btn-danger\">Update record</button></a></td></tr>";                        
                        $("#activities").append(htmlForTable);
                        htmlForTable = '';
                    }
                    });
                }
            });


        });

});


</script>


</head>

<body background="https://hbee178.files.wordpress.com/2013/06/white-blue-effect-backgrounds-for-powerpoint.jpg?" style="background-size:cover">

    <?php
include_once("functions.php");
?>
<div class="alert alert-success" style="display: none" id="successDiv">
  <strong>Success!</strong> Indicates a successful or positive action.
</div>

<div class="alert alert-danger" style="display: none" id="errorDiv">
  <strong>Error!</strong> Indicates a successful or positive action.
</div>


<div class="container">
    <div class="row" style="margin: 20px">
        <div class="col">
            <form action="?register_user" method="POST" class="form-horizontal">
                <legend>
                    <b>New to this site?</b>
                </legend>
                <fieldset>

                        <div class="form-group">
                        <label for="username" class="control-label col-sm">User Name: </label>
                        <div class="col-sm-10">
                        <input type="text" size="30" name="username" class="form-control">
                        </div>
                        </div>

                            <div class="form-group">
                            <label for="firstname" class="control-label col-sm">First Name: </label>
                            <div class="col-sm-10">
                            <input type="text" size="30" name="firstname" id="firstname" class="form-control">
                            </div>
                            </div>

                            <div class="form-group">
                            <label for="lastname" class="control-label col-sm">Last Name: </label>
                            <div class="col-sm-10">
                            <input type="text" size="30" name="lastname" id="lastname" class="form-control">
                            </div>
                            </div>


                            <div class="form-group">
                            <label for="password" class="control-label col-sm">Password: </label>
                            <div class="col-sm-10">
                            <input type="password" size="30" name="password" class="form-control">
                            </div>
                            </div>

                            <div class="form-group">
                            <label for="confirmPassword" class="control-label col-sm">Repeat Password: </label>
                            <div class="col-sm-10">
                            <input type="password" size="30" name="confirmPassword" class="form-control">
                            </div>
                            </div>

                            <div class="form-group">
                            <label for="mail"  class="control-label col-sm">Mail: </label>
                            <div class="col-sm-10">
                            <input type="text" size="30" name="mail" id="mail" class="form-control">
                            </div>
                            </div>
                            
                            <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" name="submit" class="btn btn-success">Register</button>
                            </div>
                            </div>
                        

                </fieldset>
            </form>
        </div>


        <div class="col">
            <form action="?login_user" method="POST">
                <legend>
                    <b>Already registered? Login!</b>
                </legend>
                <fieldset>

                    <div class="form-group">
                    <label for="username" class="control-label col-sm">Username: </label>
                    <div class="col-sm-10">
                        <input type="text" size="30" name="username" class="form-control" id="logUserName">
                        </div>
                    </div>

                    <div class="form-group">
                    <label for="password" class="control-label col-sm">Password:</label>
                    <div class="col-sm-10">
                    <input type="password" size="30" name="password" class="form-control" id="logPassWord">
                    </div>
                    </div>
                    </form>

                    <!--<input type="checkbox" name="alreadyregistered" value="true" onclick="changeState('this')" id="isregistered"> I am already registered.-->
                    <!--<input id='hiddenCheckbox' type='hidden' value='false' name='alreadyregistered'> -->
            	    <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="submit" class="btn btn-success" id="loginButton">Login</button>
                    </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    </div>
</body>

</html>