<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>iTech Empires Demo - PHP File upload using jQuery</title>

    <!-- Bootstrap CSS File -->
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" />
    <script src="../js/jquery-3.3.1.min.js"></script>
</head>

<body>
    <script>
        function uploadFile() {
            if ($("#file_to_upload").val() != "") {
                var file_data = $('#file_to_upload').prop('files')[0];
                var form_data = new FormData();

                form_data.append('file', file_data);

                $.ajax({
                    url: 'uploadFile.php', // point to server-side PHP script
                    dataType: 'text', // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function (data) {
                        // get server responce here
                        alert(data);
                        // clear file field
                        $("#file_to_upload").val("");
                    }
                });
            } else {
                alert("Please select file!");
            }
        }
    </script>
    <!-- Content Section -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>PHP File Upload using jQuery</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-0">
                <div class="form-group">
                    <form enctype="multipart/form-data">
                        <label>Select File to Upload</label>
                        <input id="file_to_upload" type="file" class="form-control" />
                    </form>
                </div>
                <div class="form-group">
                    <button onclick="uploadFile()" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /Content Section -->

    <!-- Jquery JS file -->
    <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>

    <!-- Bootstrap JS file -->
    <script type="text/javascript" src="bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>

    <!-- Custom JS file -->
    <script type="text/javascript" src="js/script.js"></script>
</body>

</html>