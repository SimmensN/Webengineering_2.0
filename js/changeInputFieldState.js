        function changeState(checkboxID) {

        
        var checkbox = document.getElementById("isregistered");
        var firstname = document.getElementById("firstname");
        var lastname = document.getElementById("lastname");
        var mail = document.getElementById("mail");
        var submitbutton = document.getElementById("submitbutton");
        var hidden = document.getElementById('hiddenCheckbox')
        //updateToggle = checkbox.checked ? toggle.disabled=true : toggle.disabled=false;
        if(checkbox.checked){
            firstname.disabled=true;
            firstname.value="-Not required for login-";
            firstname.style.backgroundColor="#D3D3D3";

            lastname.disabled=true;
            lastname.value="-Not required for login-";
            lastname.style.backgroundColor="#D3D3D3";

            mail.disabled=true;
            mail.style.backgroundColor="#D3D3D3";
            mail.value="-Not required for login-";
            submitbutton.value="Login";
            hidden.disabled = "true";
        }else{
            firstname.disabled=false;
            firstname.style.backgroundColor="whitesmoke";
            firstname.value="";

            lastname.disabled=false;
            lastname.style.backgroundColor="whitesmoke";
            lastname.value="";

            mail.disabled=false;
            mail.style.backgroundColor="whitesmoke";
            mail.value="";
            submitbutton.value="Register";
            hidden.disabled = "false";
        }
        }
