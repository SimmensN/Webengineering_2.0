<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1518273800456" ID="ID_175516735" MODIFIED="1518275723935" TEXT="Plot Kiera Hale - Chapter 16 &quot;Hagrid&quot;">
<node CREATED="1518274246040" ID="ID_265094686" MODIFIED="1518274266868" POSITION="right" TEXT="Date and Location: One week later in the great hall"/>
<node CREATED="1518274267283" ID="ID_1975966944" MODIFIED="1518274337081" POSITION="right" TEXT="Setting: Kiera is reading the newspaper, Nott is doing the same; some Slytherins do their homework at the table, Draco and his goons talk (fun) about other students"/>
<node CREATED="1518274337895" ID="ID_1322555967" MODIFIED="1518274354711" POSITION="right" TEXT="Kiera reads the article about Hagrid&apos;s real mother and that he is a half-giant"/>
<node CREATED="1518274357726" ID="ID_1632521357" MODIFIED="1518274391664" POSITION="right" TEXT="Somehow, Malfoy sees the article as well / knows about this and spotts about half-breeds"/>
<node CREATED="1518274393645" ID="ID_943173486" MODIFIED="1518274417809" POSITION="right" TEXT="Kiera finishes her breakfast in a hurry, then joggs to Hagrid&apos;s hut"/>
<node CREATED="1518274422168" ID="ID_696327795" MODIFIED="1518274441308" POSITION="right" TEXT="At first, Hagrid does&apos;t want to open, but after some time, he let&apos;s her in"/>
<node CREATED="1518274441736" ID="ID_1333127666" MODIFIED="1518274456382" POSITION="right" TEXT="On his table is the daily prophet"/>
<node CREATED="1518274456883" ID="ID_1815435744" MODIFIED="1518274480291" POSITION="right" TEXT="Kiera spots page with article and comforts Hagrid"/>
<node CREATED="1518274481273" ID="ID_218423032" MODIFIED="1518274497387" POSITION="right" TEXT="contemplates telling him her secret, chooses otherwise"/>
<node CREATED="1518274497888" ID="ID_950177841" MODIFIED="1518275071404" POSITION="right" TEXT="leaves through the rear exit when Hagrid spots some of the other students"/>
<node CREATED="1518275153622" ID="ID_146181269" MODIFIED="1518275182325" POSITION="right" TEXT="Raue-Pritsche: grey hair, markant chin"/>
<node CREATED="1518275182948" ID="ID_1682909633" MODIFIED="1518275298371" POSITION="right" TEXT="Kiera visits Hagrid secretely every evening"/>
<node CREATED="1518275605944" ID="ID_603377891" MODIFIED="1518275618784" POSITION="right" TEXT="eventually tells her about his father"/>
<node CREATED="1518275724570" ID="ID_60711512" MODIFIED="1518275744739" POSITION="left" TEXT="One of the meetings: Dumbledore knocks at the hut and enters; sees Kiera with Hagrid"/>
<node CREATED="1518275756942" ID="ID_856929077" MODIFIED="1518275769911" POSITION="left" TEXT="she stays silent during the whole visit"/>
<node CREATED="1518275774644" ID="ID_487854621" MODIFIED="1518275799840" POSITION="left" TEXT="Dumbledore asks Kiera why they haven&apos;t helped them, Kiera says it&apos;s been to cold (curt)"/>
<node CREATED="1518275801244" ID="ID_1860065477" MODIFIED="1518275803569" POSITION="left" TEXT="leaves then"/>
</node>
</map>
