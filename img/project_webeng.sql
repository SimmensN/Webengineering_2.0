-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 28. Apr 2018 um 16:13
-- Server-Version: 10.1.30-MariaDB
-- PHP-Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `project_webeng`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(512) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `duration` int(11) NOT NULL DEFAULT '0',
  `description` varchar(1024) DEFAULT NULL,
  `length` int(255) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `file` varchar(514) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `activities`
--

INSERT INTO `activities` (`id`, `userid`, `createdate`, `name`, `location`, `duration`, `description`, `length`, `visible`, `file`) VALUES
(29, 10, '2018-04-05 07:02:18', 'Testactivity', 'Mannheim', 0, 'TestDescription', 4, 1, '../img/7 (3) (2).jpg'),
(30, 10, '2018-04-05 10:32:10', 'test02', 'mannheim', 0, 'testjkaÃ¶fjdaÃ¶', 5, 0, '../img/13cecc35f7af.png'),
(32, 10, '2018-04-05 11:00:21', 'Test099', 'Neuburg', 0, 'skdafÃ¶j', 99, 1, '../img/19623110_1926901150901814_8886226832219701248_n.jpg'),
(33, 13, '2018-04-05 11:05:12', 'Simoon Ã¶Ã¤ad', 'Ã¶jÃ¶jkÃ¶l', 0, 'Ã¶jkÃ¶jÃ¶', 1, 0, '../img/20181257_130890120843743_4335820286185177088_n (1).jpg'),
(34, 13, '2018-04-05 11:13:07', 'BlockTest', 'Mannheim', 0, 'TestBlock', 2, 1, '../img/19954962_1971480579737679_2674011733290385408_n.jpg'),
(35, 13, '2018-04-05 11:13:46', 'Block002', 'Mannheim', 30, 'tesjÃ¶t', 999, 0, '../img/19436763_116232875654665_5151156111490940928_n.jpg'),
(36, 13, '2018-04-05 11:43:46', 'DurationTest', 'Duration', 33, 'Duration', 6, 0, '../img/19367931_1022057967925295_8737119217108647936_n (1).jpg');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `logindata`
--

CREATE TABLE `logindata` (
  `ID` int(11) NOT NULL,
  `FirstName` varchar(255) CHARACTER SET latin1 NOT NULL,
  `LastName` varchar(255) CHARACTER SET latin1 NOT NULL,
  `Password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `Mail` varchar(255) CHARACTER SET latin1 NOT NULL,
  `CreatedTS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UserName` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `logindata`
--

INSERT INTO `logindata` (`ID`, `FirstName`, `LastName`, `Password`, `Mail`, `CreatedTS`, `UserName`) VALUES
(10, '', '', '$2y$10$6/H/gDQeVMNLikjNm7esseN00s/65N1kaUKgFoc6v9r427kTemHMS', 'simon@gmx.de', '2018-04-05 07:01:17', 'Simon'),
(13, 'a', 'a', '$2y$10$0fPrtqjs/Zostc5rW7Ryde4VlbW3qeMPDfMH5WiWPfcdzfNywP03i', 'a@gmx.de', '2018-04-05 10:17:24', 'a');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`);

--
-- Indizes für die Tabelle `logindata`
--
ALTER TABLE `logindata`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT für Tabelle `logindata`
--
ALTER TABLE `logindata`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `activities`
--
ALTER TABLE `activities`
  ADD CONSTRAINT `activities_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `logindata` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
