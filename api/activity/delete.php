<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
 
// include database and object file
include_once '../config/database.php';
include_once '../objects/activity.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare product object
$activity = new  Activity($db);
 
$name = $_POST['acName']; 
$desc = $_POST['acDesc']; 
$length = $_POST['acLength']; 
$city = $_POST['acCity'];  
$id = $_POST['id'];  
 
// set product id to be deleted
$activity->id = $id;
$activity->name = $name;
$activity->length = $length;
$activity->description = $desc;
$activity->location = $city;
 
// delete the product
if($activity->delete()){
    echo '{';
        echo '"message": "Product was deleted."';
    echo '}';
}
 
// if unable to delete the product
else{
    echo '{';
        echo '"message": "Unable to delete object."';
    echo '}';
}
?>