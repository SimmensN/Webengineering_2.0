<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/activity.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare product object
$activity = new Activity($db);
 
// get id of product to be edited
 
// set ID property of product to be edited


$name = $_POST['acName']; 
$desc = $_POST['acDesc']; 
$length = $_POST['acLength']; 
$city = $_POST['acCity']; 
$duration = $_POST['acDuration']; 
$id = $_POST['id'];  
 

$activity->id = $id;
// set product property values
$activity->name = $name;
$activity->length = $length;
$activity->description = $desc;
$activity->location = $city;
$activity->duration = $duration;
 
// update the product
if($activity->update()){
    echo '{';
        echo '"message": "Product was updated."';
    echo '}';
}
 
// if unable to update the product, tell the user
else{
    echo '{';
        echo '"message": "Unable to update product."';
    echo '}';
}
?>