<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/user.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare product object
$user = new User($db);
 
// get id of product to be edited
 
// set ID property of product to be edited
session_start();
$id = $_SESSION['id'];  
$username = $_POST['us_username']; 
$firstname = $_POST['us_firstname'];
$lastname = $_POST['us_lastname'];
$mail = $_POST['us_mail']; 
//$oldpassword = $_POST['us_oldpass']; 
$newpassword = $_POST['us_newpass'];  

 

$user->id = $id;
// set product property values
$user->username = $username;
$user->mail = $mail;
$user->firstname = $firstname;
$user->lastname = $lastname;
//$user->oldpassword = $oldpassword;
$user->newpassword = $newpassword;
 
// update the product
if($user->update()){
    echo '{';
        echo '"message": "User was updated."';
    echo '}';
}
 
// if unable to update the product, tell the user
else{
    echo '{';
        echo '"message": "Unable to update User."';
    echo '}';
}
?>