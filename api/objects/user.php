<?php
class User{
 
    // database connection and table name
    private $conn;
    private $table_name = "logindata";
 
    // object properties
    public $username;
    public $firstname;
    public $lastname;
    public $mail;
    //public $oldpassword;
    public $newpassword;
    public $id;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    function update(){
 
        // update query
        $query = "UPDATE
                    " . $this->table_name . " p
                SET
                    p.username = :username,
                    p.firstname = :firstname,
                    p.lastname = :lastname,
                    p.password = :password,
                    p.mail = :mail
                WHERE
                    p.id = :id";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->username=htmlspecialchars(strip_tags($this->username));
        $this->firstname=htmlspecialchars(strip_tags($this->firstname));
        $this->lastname=htmlspecialchars(strip_tags($this->lastname));
        $this->mail=htmlspecialchars(strip_tags($this->mail));
        $this->id=htmlspecialchars(strip_tags($this->id));
        $this->newpassword=htmlspecialchars(strip_tags($this->newpassword));


        $this->newpassword = password_hash($this->newpassword, PASSWORD_DEFAULT);
        
        // bind new values
        $stmt->bindParam(':username', $this->username);
        $stmt->bindParam(':firstname', $this->firstname);
        $stmt->bindParam(':lastname', $this->lastname);
        $stmt->bindParam(':mail', $this->mail);
        $stmt->bindParam(':password', $this->newpassword);
        $stmt->bindParam(':id', $this->id);

 
        // execute the query
        if($stmt->execute()){
            return true;
        }
     
        return false;
    }
}