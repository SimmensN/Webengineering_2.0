<?php
class Activity{
 
    // database connection and table name
    private $conn;
    private $table_name = "activities";
 
    // object properties
    public $id;
    public $userid;
    public $createdate;
    public $name;
    public $location;
    public $description;
    public $length;
    public $visible;
    public $duration;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    function read(){
 
        // select all query
        $query = "SELECT
                    p.id, p.userid, p.name, p.location, p.description, p.length, p.visible, l.username, p.createdate, p.duration, p.file
                FROM
                    " . $this->table_name . " p
                    JOIN logindata l on p.userid=l.id;";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // execute query
        $stmt->execute();
     
        return $stmt;
    }

    function readMyActivities($temp){
 
        // select all query
        $query = "SELECT
                    p.id, p.userid, p.name, p.location, p.description, p.length, p.visible, l.username, p.createdate, p.duration, p.file
                FROM
                    " . $this->table_name . " p
                    JOIN logindata l on p.userid=l.id
                    WHERE l.username Like '" . $temp . "';";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // execute query
        $stmt->execute();
     
        return $stmt;
    }

    function update(){
 
        // update query
        $query = "UPDATE
                    " . $this->table_name . " p
                SET
                    p.name = :name,
                    p.location = :location,
                    p.description = :description,
                    p.length = :length,
                    p.duration = :duration
                WHERE
                    id = :id";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->location=htmlspecialchars(strip_tags($this->location));
        $this->description=htmlspecialchars(strip_tags($this->description));
        $this->length=htmlspecialchars(strip_tags($this->length));
        $this->id=htmlspecialchars(strip_tags($this->id));
        $this->duration=htmlspecialchars(strip_tags($this->duration));
        
        // bind new values
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':location', $this->location);
        $stmt->bindParam(':description', $this->description);
        $stmt->bindParam(':length', $this->length);
        $stmt->bindParam(':id', $this->id);
        $stmt->bindParam(':duration', $this->duration);

 
        // execute the query
        if($stmt->execute()){
            return true;
        }
     
        return false;
    }

    function delete(){
 
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE id = ?";
     
        // prepare query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));
     
        // bind id of record to delete
        $stmt->bindParam(1, $this->id);
     
        // execute query
        if($stmt->execute()){
            return true;
        }
     
        return false;
         
    }
}